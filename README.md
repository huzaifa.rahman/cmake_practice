# Cmake_practice

This repo is to practice basic functionality of cmake capabilities

## Task 1: Compile single file with cmake 

- I configured the project by running `cmake ..` in build directory.
- Then ran `cmake --build .` to build/compile in the same directory which will produce the executable.

`CMakeLists.txt` in this case simply contains three things:

- Minimum cmake version requirement defined using `cmake_minimum_required(VERSION <version_number>)`
- Defined project name using `project(hello VERSION <version_number>)`. Version specification is optional.
- Add executable using `add_executable(<exe> <list_of_src_files>)`.

## Task 2: Make a static library it with cmake 

To make a new library in cmake I used `add_library(<lib_name> <list_of_src_files>)`. This will create a static library by default. 

## Task 3: Link the static library with exec 

To link the new library in cmake I used `target_link_libraries(<exec_name> <link_type> <library_name>)` where link type is PUBLIC which means the following library will be used for linking to the current target and providing the interface to the other targets that have dependencies on the current target. 
This command will create a static library by default.  

## Task 4: Build in Debug and Release mode

I used `cmake -DCMAKE_BUILD_TYPE=<build_type> <path_to_root_CMakeLists.txt>` to define build type in cmake configuration step. `set_target_properties(<target_name> PROPERTIES DEBUG_POSTFIX "<any_string>")` to distingish debug target from release target. 

## Task 5: Enable all warnings

I have done this in two ways:

- Using `target_compile_options(<target_name> <link_type> <compilation_options>)` where target name is my executable name.
- Using `set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} <compilation_flags>")` which will set C++ compilation flags.

## Task 6 and Task 7: Push repo and use `.gitignore` 

I have used `.gitignore` while uploading repo to ignore the complete `build` directory.





