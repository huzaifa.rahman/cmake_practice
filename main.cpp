#include "hello.hpp"
#include "nlohmann/json.hpp"
#include <iostream>
#include "config.h"

using namespace std;

int main()
{
    // int a;
    say_hello();
    printf("%s\n", SOME_STRING);

    nlohmann::json j = {
        {"pi", 3.141},
        {"happy", true},
        {"name", "Niels"},
    };

    string s = j.dump(4);
    cout << s << endl;
}